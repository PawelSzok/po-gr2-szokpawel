
import java.util.BitSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Stack;

public class Lab12 {
    public static <T> void redukuj(LinkedList<T> pracownicy, int n) {
        for (int i = n - 1; i < pracownicy.size(); i += n - 1) {
            pracownicy.remove(i);
        }
    }

    public static <T> void odwroc(LinkedList<T> lista) {
        LinkedList<T> temp = new LinkedList<>(lista);
        int j = 0;
        for (int i = lista.size(); i >= 0; i--) {
            lista.set(j, temp.get(i));
            j++;
        }
    }

    public static String slowa_w_zdaniu(String zdanie){
    String[] slowa= zdanie.split(" ");
    Stack<String> master_yoda= new Stack<>();
    StringBuilder odwrocone= new StringBuilder();
        for(String slowo: slowa){
        master_yoda.push(slowo);
        if(slowo.endsWith(".")){
            while(!master_yoda.empty()){
                StringBuilder slowo_odwrocone = new StringBuilder();
                slowo_odwrocone.append(master_yoda.pop());
                if(master_yoda.empty()){
                    slowo_odwrocone.setCharAt(0,Character.toLowerCase(slowo_odwrocone.charAt(0)));
                    odwrocone.append(slowo_odwrocone);
                    odwrocone.append(". ");
                }
                else if(slowo_odwrocone.toString().equals(slowo)){
                    slowo_odwrocone.setCharAt(0,Character.toUpperCase(slowo_odwrocone.charAt(0)));
                    odwrocone.append(slowo_odwrocone, 0, slowo_odwrocone.length()-1);
                    odwrocone.append(" ");
                }
                else{
                    odwrocone.append(slowo_odwrocone);
                    odwrocone.append(" ");
                }
            }
        }
    }
        return odwrocone.toString();
    }
    public static void cyfry(int liczba){
        Stack<Integer> cyfry = new Stack<>();
        while(liczba != 0) {
            cyfry.push(liczba % 10);
            liczba /= 10;
        }
        while (!cyfry.empty()) {
            System.out.println(cyfry.pop() + " ");
        }
    }
    public static void Erastotenes(int n){
        BitSet b = new BitSet(n + 1);
        for (int j = 2; j <= n; ++j) {
            b.set(j);
        }
        int j = 2;
        while (j * j <= n) {
            if (b.get(j)) {
                int k = 2 * j;
                while (k <= n) {
                    b.clear(k);
                    k += j;
                }
            }
            ++j;
        }
        int[] primes= b.stream().toArray();
        for(int x: primes){
            System.out.print(x+" ");
        }
        System.out.println();
    }
    public static <T extends Iterable<?>> void print(T object ){
        Iterator<?> it= object.iterator();
        while (it.hasNext()){
            System.out.print(it.next());
            if (it.hasNext()){
                System.out.print(", ");
            }
        }
        System.out.println();
    }
    public static void main(String[] args){
        LinkedList<String> pracownicy = new LinkedList<>();
        pracownicy.add("Andrzej");
        pracownicy.add("Piotrek");
        pracownicy.add("Karolina");
        pracownicy.add("Bartosz");
        pracownicy.add("Kasia");
        pracownicy.add("Dominika");
        pracownicy.add("Filip");
        pracownicy.add("Iza");
        LinkedList<Integer> liczby = new LinkedList<>();
        Lab12.redukuj(pracownicy,2);
        System.out.println(pracownicy);
        liczby.add(1);
        liczby.add(2);
        liczby.add(3);
        liczby.add(4);
        liczby.add(5);
        liczby.add(6);
        liczby.add(7);
        Lab12.redukuj(liczby,2);
        System.out.println(liczby);
        System.out.println(slowa_w_zdaniu("Ala ma kota. Jej kot lubi myszy."));
        Erastotenes(100);
        print(pracownicy);
    }
}