import java.time.LocalDate;

public class Klient implements Cloneable , Comparable<Klient>{
    private String nazwa;
    private int id;
    private LocalDate dataZakupy;
    private double rachunek;

    public Klient(String nazwa, int id, LocalDate dataZakupu, double rachunek) {
        this.nazwa = nazwa;
        this.id = id;
        this.dataZakupy = dataZakupu;
        this.rachunek = rachunek;
    }

    public String getNazwa() {
        return nazwa;
    }

    public int getId() {
        return id;
    }

    public LocalDate getDataZakupy() {
        return dataZakupy;
    }

    public double getRachunek() {
        return rachunek;
    }

    @Override
    public int compareTo(Klient o) {
        if(o.getDataZakupy().compareTo(this.dataZakupy)!=0){
            return o.getDataZakupy().compareTo(this.dataZakupy)*-1;
        }
        if(o.getNazwa().compareTo(this.nazwa)!=0){
            return o.getNazwa().compareTo(this.nazwa)*-1;
        }
        if(o.getRachunek()<this.rachunek){
            return 1;
        }else if(o.getRachunek()>this.rachunek){
            return -1;
        }
        return 0;
    }
    @Override
    public String toString(){
        return "Id : " + id + " nazwa klienta: " + nazwa + " dataZakupu: " + dataZakupy + " rachunek: " + rachunek;
    }
}