import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.TreeMap;

public class ObslugaKlienta{
    public static double procentRabatu;
    private ArrayList<Klient> klienci;

    public ObslugaKlienta( ArrayList<Klient> klienci) {
        this.klienci = klienci;
    }

    public ArrayList<Klient> getKlienci() {
        return klienci;
    }

    public void set() {
        this.procentRabatu = 0.05;
    }
    public static double discountAmount(Klient k){
        if(k.getRachunek()>300){
            return k.getRachunek()*procentRabatu;
        }
        return 0;
    }
    public static TreeMap DiscoutMap(ArrayList<Klient>klist){
        TreeMap<Integer, String> mapa = new TreeMap<Integer, String>();
        String str;
        for (Klient k:klist) {
            if(discountAmount(k)>0){
                str=k.getNazwa()+" " +discountAmount(k);
                mapa.put(k.getId(),str);
            }
        }
        return mapa;
    }

    public static void main(String[] args){
        ArrayList<Klient> klienci = new ArrayList<>();
        klienci.add(new Klient("Marzena",1, LocalDate.of(2000,1,30),1000));
        klienci.add(new Klient("Iwona",2, LocalDate.of(2000,1,30),3000));
        klienci.add(new Klient("Kacper",3, LocalDate.of(1950,10,3),15000));
        klienci.add(new Klient("Bartosz",4, LocalDate.of(1410,6,10),3000));
        klienci.add(new Klient("Kacper",5, LocalDate.of(2010,2,14),70));
        ObslugaKlienta grupa = new ObslugaKlienta(klienci);
        System.out.println(grupa.getKlienci());
        Collections.sort(grupa.getKlienci());
        grupa.getKlienci().sort(Klient::compareTo);
        System.out.println(grupa.getKlienci());
        grupa.set();
        System.out.println(ObslugaKlienta.discountAmount(grupa.getKlienci().get(1)));
        System.out.println(grupa.DiscoutMap(grupa.getKlienci()));

    }
}