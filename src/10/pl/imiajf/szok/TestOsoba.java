package pl.imiajf.szok;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;

public class TestOsoba{
    public static void main(String[] args){
        ArrayList<Osoba> listaOsob= new ArrayList<>();
        listaOsob.add(new Osoba("Nowak", LocalDate.of(1950,6,12)));
        listaOsob.add(new Osoba("Kowalski", LocalDate.of(1995,10,3)));
        listaOsob.add(new Osoba("Kowalski", LocalDate.of(1987,1,26)));
        listaOsob.add(new Osoba("Trabka", LocalDate.of(1950,6,12)));
        listaOsob.add(new Osoba("Szok", LocalDate.of(2000,5,23)));

        System.out.println(listaOsob.get(0));
        System.out.println(listaOsob.get(0).equals(listaOsob.get(3)));
        System.out.println(listaOsob.get(1).equals(listaOsob.get(2)));
        System.out.println(listaOsob);
        Collections.sort(listaOsob);
        System.out.println(listaOsob);

    }
}