package pl.imiajf.szok;


import java.time.LocalDate;

public class Student extends Osoba implements Cloneable, Comparable<Osoba>{
    public Student(String nazwisko, LocalDate dataUrodzenia, double sredniaOcen){
        super(nazwisko, dataUrodzenia);
        this.sredniaOcen = sredniaOcen;
    }
    private double sredniaOcen;

    public String toString(){
        return super.toString() + " srednia ocen "+sredniaOcen;
    }

    @Override
    public int compareTo(Osoba osoba){
        int wynik = super.compareTo(osoba);
        if((osoba instanceof Student) && (wynik == 0)){
            return Double.compare(this.sredniaOcen,((Student)osoba).sredniaOcen);
        }
        return wynik;
    }


}