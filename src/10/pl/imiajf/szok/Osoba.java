package pl.imiajf.szok;
import java.time.LocalDate;

public class Osoba implements Cloneable, Comparable<Osoba>{
    private String nazwisko;
    private LocalDate dataUrodzenia;

    public Osoba(String nazwisko, LocalDate dataUrodzenia){
        this.nazwisko = nazwisko;
        this.dataUrodzenia = dataUrodzenia;
    }

    public String getNazwisko(){
        return nazwisko;
    }
    public LocalDate getDataUrodzenia(){
        return dataUrodzenia;
    }
    @Override
    public String toString(){
        return "Nazwa klasy: " + getClass().getSimpleName() + " , [nazwisko: " + nazwisko + " data urodzenia: " + dataUrodzenia +  "]";
    }
    @Override
    public boolean equals(Object obiekt){
        Osoba osoba = (Osoba) obiekt;
        return (osoba.nazwisko.equals(this.nazwisko) && osoba.dataUrodzenia.equals(this.dataUrodzenia));
    }
    @Override
    public int compareTo(Osoba osoba){
        int porownanie = this.nazwisko.compareTo(osoba.nazwisko);
        if(porownanie == 0){
            porownanie = this.dataUrodzenia.compareTo(osoba.dataUrodzenia);
        }
        return porownanie;
    }
}

