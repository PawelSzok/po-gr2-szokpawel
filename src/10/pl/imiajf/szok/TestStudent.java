
package pl.imiajf.szok;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;

public class TestStudent{
    public static void main(String[] args){
        ArrayList<Student> listaStudentow = new ArrayList<>();
        listaStudentow.add(new Student("Nowak", LocalDate.of(1998,12,2),3.14));
        listaStudentow.add(new Student("Kowalski", LocalDate.of(1993,10,12),3.14));
        listaStudentow.add(new Student("Szok", LocalDate.of(2000,5,23),3.14));
        listaStudentow.add(new Student("Majewski", LocalDate.of(1993,10,12),3.14));
        listaStudentow.add(new Student("Nowak", LocalDate.of(1995,11,16),3.14));

        System.out.println(listaStudentow);
        Collections.sort(listaStudentow);
        System.out.println(listaStudentow);
    }
}