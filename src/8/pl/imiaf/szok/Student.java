package pl.imiaf.szok;

import java.time.LocalDate;

class Student extends Osoba
{
    public Student(String nazwisko, String[]imie, LocalDate dataUrodzneia, Boolean plec, String kierunek, double sredniaOcen)
    {
        super(nazwisko,imie,dataUrodzneia, plec);
        this.kierunek = kierunek;
        this.sredniaOcen = sredniaOcen;
    }

    public String getOpis()
    {
        return "kierunek studiów: " + kierunek;
    }
    public double getSredniaOcen(){
        return sredniaOcen;
    }
    public String getKierunek(){
        return kierunek;
    }

    private String kierunek;
    private double sredniaOcen;
}
