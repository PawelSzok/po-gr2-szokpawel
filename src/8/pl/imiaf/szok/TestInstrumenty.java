package pl.imiaf.szok;

import java.util.ArrayList;
import java.time.LocalDate;

public class TestInstrumenty{
    public static void main(String[] args){
        ArrayList<Instrument> okriestra = new ArrayList<>();
        okriestra.add(new Flet("Malinowej",LocalDate.of(1993,2,12)));
        okriestra.add(new Fortepian("Szpoin",LocalDate.of(1693,4,24)));
        okriestra.add(new Skrzypce("Truskawkowe",LocalDate.of(2019,1,2)));
        okriestra.add(new Flet("Okular",LocalDate.of(1410,7,22)));
        okriestra.add(new Skrzypce("Wrketak",LocalDate.of(2010,10,29)));

        for(Instrument element:okriestra){
            element.dzwiek();
        }
        for(Instrument element:okriestra){
            System.out.println(element);
        }
    }
}