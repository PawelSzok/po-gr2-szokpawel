package pl.imiaf.szok;

import java.time.LocalDate;

public class Skrzypce extends Instrument{
    public Skrzypce(String producent, LocalDate data){
        super(producent, data);
    }
    void dzwiek() {
        System.out.println("Skrz...");
    }
}

