package pl.imiaf.szok;

import java.time.LocalDate;

public abstract class Osoba {
    public Osoba(String nazwisko, String[] imie, LocalDate dataUrodzenia, Boolean plec)
    {
        this.nazwisko = nazwisko;
        this.imie = imie;
        this.dataUrodzenia = dataUrodzenia;
        this.plec = plec;
    }

    public abstract String getOpis();

    public String getNazwisko()
    {
        return nazwisko;
    }
    public StringBuilder getImie(){
        StringBuilder fullName = new StringBuilder();
        for(String element:imie){
            fullName.append(element);
            fullName.append(" ");
        }
        return fullName;
    }
    public LocalDate getDataUrodzenia(){
        return dataUrodzenia;
    }
    public String getPlec(){
        if (plec)
            return "Mezczyzna";
        else
            return "Kobieta";
    }

    private String nazwisko;
    private String[] imie;
    private LocalDate dataUrodzenia;
    private Boolean plec;
}