package pl.imiaf.szok;

import java.time.LocalDate;

public abstract class Instrument{
    private String producent;
    private LocalDate rokProdukcji;

    public Instrument(String producent, LocalDate data){
        this.producent = producent;
        this.rokProdukcji = data;
    }
    public String getProducent(){
        return this.producent;
    }
    public LocalDate getRokProdukcji(){
        return this.rokProdukcji;
    }

    abstract void dzwiek();

    @Override
    public boolean equals(Object anObject){
        return this.toString().equals(anObject.toString());
    }

    @Override
    public String toString(){
        return "Instrument: "+this.getClass().getSimpleName()+", Rok produkcji: "+this.rokProdukcji+", Producent: "+this.producent;
    }

}