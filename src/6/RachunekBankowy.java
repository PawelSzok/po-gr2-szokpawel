public class RachunekBankowy{
    private static double rocznaStopaOprocentowania;
    private double saldo;
    public RachunekBankowy(double n){
        this.saldo = n;
    }
    public void obliczMiesieczneOdestki(){
        this.saldo += (this.saldo * this.rocznaStopaOprocentowania) / 12;
    }
    public static void setRocznaStopaOprocentowania(double n){
        rocznaStopaOprocentowania = n;
    }
    public double getSaldo(){
        return this.saldo;
    }

}