
public class IntegerSet{
     private boolean[] zbior ;
     public IntegerSet(){
         this.zbior = new boolean[100];
     }
     public static IntegerSet union(IntegerSet pierwszy, IntegerSet drugi){
         IntegerSet nowy = new IntegerSet();
         for (int i = 0; i < 100; i++) {
             if(pierwszy.zbior[i] || drugi.zbior[i]) nowy.zbior[i] = true;
         }
         return nowy;
     }
     public static IntegerSet intersection(IntegerSet pierwszy, IntegerSet drugi){
         IntegerSet nowy = new IntegerSet();
         for (int i = 0; i < 100; i++) {
             if(pierwszy.zbior[i] && drugi.zbior[i]) nowy.zbior[i] = true;
         }
         return nowy;
     }
     public void insertElement(int n){
         this.zbior[n-1] = true;
     }
     public void deleteElement(int n){
         this.zbior[n-1] = false;
     }
     @Override
    public String toString(){
         StringBuilder napis = new StringBuilder();
         for (int i = 0; i < 100; i++) {
             if(this.zbior[i]){
                 napis.append(i+1);
                 napis.append(" ");
             }
         }
         return napis.toString();
     }
     public boolean equals(IntegerSet n){
        return this.toString().equals(n.toString());
     }
}