
public class Tests{
    public static void main(String[] args ){
        //Zadanie 1
        System.out.println(" \n Zadanie 1\n");
        RachunekBankowy saver1 = new RachunekBankowy(2000.0);
        RachunekBankowy saver2 = new RachunekBankowy(3000.0);
        RachunekBankowy.setRocznaStopaOprocentowania(0.04);
        saver1.obliczMiesieczneOdestki();
        saver2.obliczMiesieczneOdestki();
        System.out.println("1: " + saver1.getSaldo() + " 2: " + saver2.getSaldo());
        RachunekBankowy.setRocznaStopaOprocentowania(0.05);
        saver1.obliczMiesieczneOdestki();
        saver2.obliczMiesieczneOdestki();
        System.out.println("1: " + saver1.getSaldo() + " 2: " + saver2.getSaldo());
        //Zadanie 2
        System.out.println(" \n Zadanie 2 \n ");
        IntegerSet setA = new IntegerSet();
        IntegerSet setB = new IntegerSet();
        setA.insertElement(2);
        setA.insertElement(4);
        setA.insertElement(15);
        setB.insertElement(15);
        setB.insertElement(40);
        setB.insertElement(60);
        System.out.println(setA);
        System.out.println(setB);
        System.out.println(IntegerSet.union(setA,setB));
        System.out.println(IntegerSet.intersection(setA,setB));
        setB.deleteElement(60);
        System.out.println(setB);
        System.out.println(setA.equals(setB));
        setB.deleteElement(40);
        setB.deleteElement(60);
        setB.insertElement(4);
        setB.insertElement(2);
        System.out.println(setA.equals(setB));
        //Zadanie 3
        System.out.println(" \n Zadanie 3 \n ");
        Pracownik[] personel = new Pracownik[3];

        // wypełnij tablicę danymi pracowników
        personel[0] = new Pracownik("Karol Cracker", 75000, 2001, 12, 15);
        personel[1] = new Pracownik("Henryk Hacker", 50000, 2003, 10, 1);
        personel[2] = new Pracownik("Antoni Tester", 40000, 2005, 3, 15);

        // zwiększ pobory każdego pracownika o 20%
        for (Pracownik e : personel) {
            e.zwiekszPobory(20);
        }

        // wypisz informacje o każdym pracowniku
        for (Pracownik e : personel) {
            System.out.print("nazwisko = " + e.nazwisko() + "\tpobory = " + e.pobory());
            System.out.printf("\tdataZatrudnienia = %tF\n", e.dataZatrudnienia());
        }
        System.out.println();

    }
}