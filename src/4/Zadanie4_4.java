
import java.math.BigInteger;

public class Zadanie4_4{
    public static void zadanie(int n){
        BigInteger wynik= new BigInteger("0");
        BigInteger mnoznik= new BigInteger("1");
        BigInteger wzrost= new BigInteger("2");
        BigInteger max= new BigInteger(String.valueOf(n));
        for(int i=0;i<n;i++){
            if(max.compareTo(mnoznik)==1){
                wynik=wynik.add(mnoznik);
                mnoznik=mnoznik.multiply(wzrost);
            }
            else{
                wynik=wynik.add(max);
            }
        }
        System.out.println("Ułożymy " + wynik.toString() + " ziaren na planszy");
    }
    public static void main( String [] args ){
        zadanie(600000000);
    }
}