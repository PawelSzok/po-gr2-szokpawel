public class Zadanie4_1_c{
    public static String middle(String str){
        int dlugosc=str.length();
        if(dlugosc%2!=0){
            return String.valueOf(str.charAt(dlugosc/2));
        }
        else {
            dlugosc= (int)(dlugosc/2);
            return str.substring(dlugosc-1,dlugosc+1);
        }
    }


    public static void main( String [] args ){
        System.out.println(middle("abcdefgh"));
    }

}