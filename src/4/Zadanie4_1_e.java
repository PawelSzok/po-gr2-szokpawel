public class Zadanie4_1_e{
    public static int[] where(String str, String subStr){
        int[] matches= new int[Zadanie4_1_b.countSubStr(str,subStr)];
        if(matches.length>0){
            String temp= str;
            matches[0]=temp.indexOf(subStr);
            for(int i=1;i<matches.length;i++){
                temp=str.substring(matches[i-1]+1);
                matches[i]=temp.indexOf(subStr)+matches[i-1]+1;
            }
        }
        return matches;
    }

    public static void main( String [] args ){
        for(int x: where("rabarbar","bar")){
            System.out.print(x+" ");
        }
    }

}