public class Zadanie4_1_f{
    public static String change(String str){
        StringBuffer napis= new StringBuffer();
        for(int i=0;i<str.length();i++){
            if(Character.isUpperCase(str.charAt(i))) napis.append(String.valueOf(str.charAt(i)).toLowerCase());
            else napis.append(String.valueOf(str.charAt(i)).toUpperCase());
        }
        return napis.toString();
    }


    public static void main( String [] args ){
        System.out.println(change("aBcDeF"));
    }

}