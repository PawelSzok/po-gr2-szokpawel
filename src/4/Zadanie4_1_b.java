

public class Zadanie4_1_b{
    public static int countSubStr(String str, String subStr) {
        int len_str = str.length();
        int len_subStr = subStr.length();
        if (str.isEmpty() || subStr.isEmpty() || len_subStr > len_str) {
            return 0;
        }
        int index = 0;
        int count = 0;
        String temp;
        while (len_str - index >= len_subStr) {
            temp = str.substring(index, index + (len_subStr));
            if (temp.equals(subStr)) count++;
            index++;
        }
        return count;
    }

    public static void main( String [] args ){
        System.out.println(countSubStr("Nananana","na"));
    }

}