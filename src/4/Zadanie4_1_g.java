public class Zadanie4_1_g{
    public static String nice(String str){
        StringBuffer napis= new StringBuffer();
        int it=1;
        for(int i=(str.length()-1);i>=0;i--){
            napis.append(str.charAt(i));
            if(it%3==0 && i!=0)  napis.append("'");
            it++;
        }
        String temp= napis.toString();
        napis=new StringBuffer();
        for(int i=temp.length()-1;i>=0;i--){
            napis.append(temp.charAt(i));
        }
        return napis.toString();
    }

    public static void main( String [] args ){
        System.out.println(nice("abcdefgh"));
    }

}