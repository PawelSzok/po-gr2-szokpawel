
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Zadanie4_2{
    public static void zadanie(String plik, char znak){
        int ilosc_znaku=0;
        try {
            File mojPlik= new File(plik);
            Scanner zczytywanie = new Scanner(mojPlik);
            while (zczytywanie.hasNextLine()) {
                String tekst = zczytywanie.nextLine();
                ilosc_znaku += (int)tekst.chars().filter(ch -> ch == znak).count();
            }
            zczytywanie.close();
        }
        catch (FileNotFoundException e) {
            System.out.println("Nie mozna odnalesc pliku");
            e.printStackTrace();
        }
        System.out.println("Znaleziono " + ilosc_znaku + " znaków "+znak);
    }
    public static void main( String [] args ){
        zadanie("C:\\Users\\Pawel\\Desktop\\sesyjka\\PO\\projekty\\src\\4\\sample.txt",'a');
    }
}