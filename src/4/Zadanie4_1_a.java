

public class Zadanie4_1_a{
    public static int countChar(String str, char c){
        int matches = 0;
        for(char character:str.toCharArray()){
            if(character==c) matches++;
        }
        return matches;
    }

    public static void main( String [] args ){
        System.out.println(countChar("Ananas",'a'));
    }

}