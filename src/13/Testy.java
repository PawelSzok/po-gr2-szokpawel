
public class Testy{
    public static void main(String [] args) {

        Egzamin egzamin = new Egzamin();
        egzamin.dodaj("Nowak", "db");
        egzamin.dodaj("Niedzielski", "bdb");
        egzamin.dodaj("Kowalski", "dst");
        egzamin.dodaj("Majewski", "bdb");
        egzamin.wypisz();
        egzamin.usun("Nowak");
        egzamin.zmien("Niedzielski","db");
        egzamin.wypisz();

        Egzaminv2 po2= new Egzaminv2();
        po2.dodaj(new Student("Mateusz","Kowalski",4),"bdb");
        po2.dodaj(new Student("Adam","Kowalski",3),"db");
        po2.dodaj(new Student("Adam","Małysz",1),"dst");
        po2.dodaj(new Student("Pawel","Kolezka",2),"ndst");
        po2.wypisz();
        po2.usun(3);
        po2.zmien(2,"dst+");
        po2.wypisz();

    }
}