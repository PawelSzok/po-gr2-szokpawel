import java.util.Objects;

public class Tasks implements Comparable<Tasks>{
    private String description;
    private double priority;

    public Tasks(String opis, double priorytet){
        this.description = opis;
        this.priority = priorytet;
    }

    public String getDescription(){
        return this.description;
    }
    public double getPriority(){
        return this.priority;
    }

    public void setDescription(String opis){
        this.description = opis;
    }
    public void setPriority(double priorytet){
        this.priority = priorytet;
    }

    @Override
    public int compareTo(Tasks o){
        int result = 0;
        if(Double.compare(this.getPriority(),o.getPriority())>0)
            result = -1;
        if(Double.compare(this.getPriority(),o.getPriority())<0)
            result = 1;
        return result;
    }
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Tasks task = (Tasks) o;
        return Double.compare(task.priority, priority) == 0 &&
                Objects.equals(description, task.description);
    }
    @Override
    public String toString(){
        return "Zadanie: "+description+" o priorytecie: "+priority;
    }
}