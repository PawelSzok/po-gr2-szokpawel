import java.util.Scanner;
import java.util.Random;
public class Zadanie3_2_b {
    public static void generuj(int tab[],int n,int minWartosc, int maxWartosc){
        for(int i=0;i<n;i++)
            tab[i]= new Random().nextInt(maxWartosc-minWartosc) + minWartosc;
    }

    public static int ileDodatnich(int tab[]){
        int dodatnie = 0;
        for (int liczba: tab){
            if(liczba>0) dodatnie++;
        }
        return dodatnie;
    }
    public static int ileUjemnych(int tab[]){
        int ujemne = 0;
        for (int liczba: tab){
            if(liczba<0) ujemne++;
        }
        return ujemne;
    }
    public static int ileZerowych(int tab[]){
        int zera = 0;
        for (int liczba: tab){
            if(liczba==0) zera++;
        }
        return zera;
    }

    public static void main ( String [] args ) {
        System.out.println("Wpisz n:");
        Scanner s = new Scanner(System.in);
        int n = s.nextInt();
        int[] tablica = new int[n];
        generuj(tablica,n,-999,999);

        System.out.println("------");
        System.out.println(ileDodatnich(tablica));
        System.out.println(ileUjemnych(tablica));
        System.out.println(ileZerowych(tablica));
    }
}