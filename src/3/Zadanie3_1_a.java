import java.util.Scanner;
import java.util.Random;
public class Zadanie3_1_a {
    public static void main ( String [] args ) {
        System.out.println("Wpisz n:");
        Scanner s = new Scanner(System.in);
        int n = s.nextInt();
        int[] tablica = new int[n];
        int parzyste=0;
        int nieparzyste=0;
        for (int i = 0; i < n; i++) {
            tablica[i] = new Random().nextInt(999+999)-999;
            if(tablica[i]%2==0) parzyste++;
            else nieparzyste++;
        }
        System.out.println("Liczba parzystych: "+parzyste+" Liczba nieparzystych: "+ nieparzyste);
    }
}