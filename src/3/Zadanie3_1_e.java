import java.util.Scanner;
import java.util.Random;
public class Zadanie3_1_e {
    public static void main ( String [] args ) {
        System.out.println("Wpisz n:");
        Scanner s = new Scanner(System.in);
        int n = s.nextInt();
        int[] tablica = new int[n];
        int dlugosc=0;
        int dlugosc_max=0;
        for (int i = 0; i < n; i++) {
            tablica[i] = new Random().nextInt(999+999)-999;
            if(tablica[i]>0)dlugosc++;
            else if(tablica[i]<=0)
            {
                if(dlugosc>dlugosc_max) dlugosc_max=dlugosc;
                dlugosc=0;
            }
        }
        System.out.println("Dlugosc  "+dlugosc_max);
    }
}