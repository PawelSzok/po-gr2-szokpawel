import java.util.Scanner;
import java.util.Random;
public class Zadanie3_2_f {
    public static void generuj(int tab[],int n,int minWartosc, int maxWartosc){
        for(int i=0;i<n;i++)
            tab[i]= new Random().nextInt(maxWartosc-minWartosc) + minWartosc;
    }

    public static void signum(int tab[]){
        for (int liczba: tab){
            if(liczba==0) System.out.println(0);
            else if(liczba>0) System.out.println(1);
            else if(liczba<0) System.out.println(-1);
        }
    }


    public static void main ( String [] args ) {
        System.out.println("Wpisz n:");
        Scanner s = new Scanner(System.in);
        int n = s.nextInt();
        int[] tablica = new int[n];
        generuj(tablica,n,-999,999);
        signum(tablica);
    }
}