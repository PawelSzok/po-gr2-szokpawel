import java.util.Scanner;
import java.util.Random;
public class Zadanie3_1_c {
    public static void main ( String [] args ) {
        System.out.println("Wpisz n:");
        Scanner s = new Scanner(System.in);
        int n = s.nextInt();
        int[] tablica = new int[n];
        int max=0;
        int ilosc_powtorzen=0;
        for (int i = 0; i < n; i++) {
            tablica[i] = new Random().nextInt(999+999)-999;
            if(i==0)max=tablica[i];
            if(tablica[i]>max)
            {
                max=tablica[i];
                ilosc_powtorzen=1;
            }
            else if(tablica[i]==max) ilosc_powtorzen++;
        }
        System.out.println("Max="+max+" Wystąpił:"+ilosc_powtorzen+" razy");
    }
}