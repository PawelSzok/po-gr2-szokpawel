import java.util.Scanner;
import java.util.Random;
public class Zadanie3_2_e {
    public static void generuj(int tab[],int n,int minWartosc, int maxWartosc){
        for(int i=0;i<n;i++)
            tab[i]= new Random().nextInt(maxWartosc-minWartosc) + minWartosc;
    }

    public static int dlugoscMaksymalnegoCiaguDodatnich(int tab[]){
        int dlugosc = 0;
        int dlugosc_max = 0;
        for (int liczba: tab){
            if(liczba>0) dlugosc++;
            else{
                if(dlugosc>dlugosc_max) dlugosc_max=dlugosc;
                dlugosc=0;
            }
        }
        return dlugosc_max;
    }


    public static void main ( String [] args ) {
        System.out.println("Wpisz n:");
        Scanner s = new Scanner(System.in);
        int n = s.nextInt();
        int[] tablica = new int[n];
        generuj(tablica,n,-999,999);

        System.out.println("maksymalna dlugosc ciagu dodatnich: "+dlugoscMaksymalnegoCiaguDodatnich(tablica));
    }
}