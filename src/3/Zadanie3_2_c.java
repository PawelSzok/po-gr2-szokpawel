import java.util.Scanner;
import java.util.Random;
public class Zadanie3_2_c {
    public static void generuj(int tab[],int n,int minWartosc, int maxWartosc){
        for(int i=0;i<n;i++)
            tab[i]= new Random().nextInt(maxWartosc-minWartosc) + minWartosc;
    }

    public static int ileMaksymalnych(int tab[]){
        int max = tab[0];
        int ilosc_max = 0;
        for (int liczba: tab){
            if(liczba==max) ilosc_max++;
            else if (liczba>max){
                max = liczba;
                ilosc_max = 1;
            }
        }
        return ilosc_max;
    }

    public static void main ( String [] args ) {
        System.out.println("Wpisz n:");
        Scanner s = new Scanner(System.in);
        int n = s.nextInt();
        int[] tablica = new int[n];
        generuj(tablica,n,-999,999);

        System.out.println("Wyrazow maksymalnych: "+ileMaksymalnych(tablica));
    }
}