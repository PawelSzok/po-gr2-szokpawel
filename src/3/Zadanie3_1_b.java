import java.util.Scanner;
import java.util.Random;
public class Zadanie3_1_b {
    public static void main ( String [] args ) {
        System.out.println("Wpisz n:");
        Scanner s = new Scanner(System.in);
        int n = s.nextInt();
        int[] tablica = new int[n];
        int dodatnie=0;
        int ujemne=0;
        int zerowe=0;
        for (int i = 0; i < n; i++) {
            tablica[i] = new Random().nextInt(999+999)-999;
            if(tablica[i]>0) dodatnie++;
            else if(tablica[i]<0) ujemne++;
            else zerowe++;
        }
        System.out.println("Liczba dodatnich: "+dodatnie+" Liczba ujemnych: "+ ujemne+" Liczba zerowych: "+ zerowe);
    }
}