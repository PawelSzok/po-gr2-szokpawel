import java.util.Scanner;
import java.util.Random;
public class Zadanie3_2_a {
    public static void generuj(int tab[],int n,int minWartosc, int maxWartosc){
        for(int i=0;i<n;i++)
            tab[i]= new Random().nextInt(maxWartosc-minWartosc) + minWartosc;
    }
    
    public static int ileNieparzystych (int tab[]){
        int ileNieparzystych=0;
        for (int liczba:tab) {
            if(liczba%2==1 || liczba%2==-1) {
                ileNieparzystych++;
            }
        }
        return ileNieparzystych;
    }

    public static int ileParzystych (int tab[]){
        int ileParzystych=0;
        for (int liczba:tab) {
            if(liczba%2==0) {
                ileParzystych++;
            }
        }
        return ileParzystych;
    }

    public static void main ( String [] args ) {
        System.out.println("Wpisz n:");
        Scanner s = new Scanner(System.in);
        int n = s.nextInt();
        int[] tablica = new int[n];
        generuj(tablica,n,-999,999);

        System.out.println("------");
        System.out.println(ileNieparzystych(tablica));
        System.out.println(ileParzystych(tablica));
    }
}