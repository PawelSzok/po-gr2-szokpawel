import java.util.Scanner;
import java.util.Random;
public class Zadanie3_1_f {
    public static void main ( String [] args ) {
        System.out.println("Wpisz n:");
        Scanner s = new Scanner(System.in);
        int n = s.nextInt();
        int[] tablica = new int[n];
        //generacja normnalnej tablicy
        for (int i = 0; i < n; i++) {
            tablica[i] = new Random().nextInt(999+999)-999;
        }
        //podmianka
        for (int i = 0; i < n; i++) {
            if(tablica[i]>0) tablica[i]=1;
            else if(tablica[i]<0) tablica[i]=-1;
        }
        //wypisanie
        for (int i = 0; i < n; i++) {
            System.out.println(tablica[i]);
        }
    }
}