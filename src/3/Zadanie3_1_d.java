import java.util.Scanner;
import java.util.Random;
public class Zadanie3_1_d {
    public static void main ( String [] args ) {
        System.out.println("Wpisz n:");
        Scanner s = new Scanner(System.in);
        int n = s.nextInt();
        int[] tablica = new int[n];
        int suma_dodatnie=0;
        int suma_ujemne=0;

        for (int i = 0; i < n; i++) {
            tablica[i] = new Random().nextInt(999+999)-999;
            if(tablica[i]>0)suma_dodatnie+=tablica[i];
            else if(tablica[i]<0)suma_ujemne+=tablica[i];
        }
        System.out.println("Suma dodatnich: "+suma_dodatnie+" suma ujemnych "+ suma_ujemne);
    }
}