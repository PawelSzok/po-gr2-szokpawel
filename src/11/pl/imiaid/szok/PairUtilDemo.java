package pl.imiaid.szok;

public class PairUtilDemo{
    public static void main(String[] args){
        Pair<String> para = new Pair<>("Pierwsze","Drugie");
        Pair<String> nowaPara = PairUtil.swap(para);
        System.out.println(nowaPara.getFirst());
        System.out.println(nowaPara.getSecond());

    }
}