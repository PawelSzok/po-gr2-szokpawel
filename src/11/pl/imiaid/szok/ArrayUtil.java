package pl.imiaid.szok;

import java.util.ArrayList;
import java.util.Collections;
import java.time.LocalDate;

public class ArrayUtil{
    public static <T extends  Comparable<? super T>> boolean isSorted(ArrayList<T> tab){
        ArrayList<T> temp = new ArrayList<>(tab);
        Collections.sort(temp);
        return tab.equals(temp);
    }
    public static <T extends Comparable<? super T>> int binSearch(ArrayList<T> tab, T a){
        if(ArrayUtil.isSorted(tab)) {
            int l = 0;
            int r = tab.size() - 1;
            while (l <= r) {
                int m = l + (r - 1) / 2;
                if (tab.get(m).equals(a)) {
                    return m;
                }
                if (tab.get(m).compareTo(a) < 0) l = m + 1;
                else r = m - 1;
            }
        }
        return -1;
    }
    public static <T extends Comparable<? super T>> void selectionSort(ArrayList<T> tab){
        for (int i=0;i<tab.size()-1;i++){
            int index = i;
            for (int j=i+1;j<tab.size();j++){
                if(tab.get(j).compareTo((tab.get(index))) < 0){
                    index = j;
                }
            }
            T mniejszy = tab.get(index);
            tab.set(index, tab.get(i));
            tab.set(i, mniejszy);
        }
    }

    public static <T extends Comparable<? super T>>  void mergeSort(ArrayList<T> tab){
        if(tab.size()>1){
            ArrayList<T> leftSide = new ArrayList<>();
            ArrayList<T> rightSide = new ArrayList<>();
            boolean doLeft = true;
            while (!tab.isEmpty()){
                if(doLeft){
                    leftSide.add((tab.remove(0)));
                }
                else{
                    rightSide.add(tab.remove(tab.size()/2));
                }
                doLeft = !doLeft;
            }
            mergeSort(leftSide);
            mergeSort(rightSide);
            while(!leftSide.isEmpty() && !rightSide.isEmpty()){
                if(leftSide.get(0).compareTo(rightSide.get(0)) <= 0){
                    tab.add(leftSide.remove(0));
                }
                else{
                    tab.add(rightSide.remove(0));
                }
            }
            if (!leftSide.isEmpty()){
                tab.addAll(leftSide);
            }
            if (!rightSide.isEmpty()){
                tab.addAll(rightSide);
            }
        }
    }
    public static void main(String[] args){
        ArrayList<LocalDate> lD = new ArrayList<>();
        lD.add(LocalDate.of(1999,8,2));
        lD.add(LocalDate.of(1999,7,2));
        lD.add(LocalDate.of(1999,8,1));
        System.out.println(ArrayUtil.isSorted(lD));
        Collections.sort(lD);
        System.out.println(ArrayUtil.isSorted(lD));
        ArrayList<Integer> i = new ArrayList<>();
        i.add(4);
        i.add(8);
        i.add(2);
        i.add(6);
        i.add(2);
        i.add(10);
        i.add(2);
        i.add(8);
        i.add(5);
        ArrayList<Integer> i_copy_1= i;
        ArrayList<Integer> i_copy_2= i;
        System.out.println(ArrayUtil.isSorted(i));
        Collections.sort(i);
        System.out.println(ArrayUtil.isSorted(i)+"\n");
        System.out.println(ArrayUtil.binSearch(i,8));
        System.out.println(ArrayUtil.binSearch(i,4));
        System.out.println(ArrayUtil.binSearch(lD,LocalDate.of(1999,8,1)));
        System.out.println(ArrayUtil.binSearch(lD,LocalDate.of(1999,1,2))+"\n");
        ArrayUtil.selectionSort(i_copy_1);
        ArrayUtil.mergeSort(i_copy_2);
        System.out.println(i_copy_1);
        System.out.println(i_copy_2);

    }
}