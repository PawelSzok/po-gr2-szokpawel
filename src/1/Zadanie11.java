package pl.edu.uwm. wmii . agnieszkazbrzezny . laboratorium00 ;
public class Zadanie11 {
    public static void main ( String [] args ) {
        System .out. println ("Wielkie to szczęście\n" +
                "nie wiedzieć dokładnie,\n" +
                "na jakim świecie się żyje.\n" +
                "\n" +
                "Trzeba by było\n" +
                "istnieć bardzo długo,\n" +
                "stanowczo dłużej\n" +
                "niż istnieje on.\n" +
                "\n" +
                "Choćby dla porównania\n" +
                "poznać inne światy.\n" +
                "\n" +
                "unieść się ponad ciało\n" +
                "które niczego tak dobrze nie umie,\n" +
                "jak ograniczać\n" +
                "i stwarzać trudności.\n" +
                "\n" +
                "Dla dobra badań,\n" +
                "jasności obrazu\n" +
                "i ostatecznych wniosków\n" +
                "wzbić się ponad czas,\n" +
                "w którym to wszystko pędzi i wiruje.\n" +
                "\n" +
                "Z tej perspektywy\n" +
                "żegnajcie na zawsze\n" +
                "szczegóły i epizody.\n" +
                "\n" +
                "Liczenie dni tygodnia\n" +
                "musiałoby się wydawać\n" +
                "czynnością bez sensu,\n" +
                "wrzucenie listu do skrzynki\n" +
                "wybrykiem głupiej młodości,\n" +
                "\n" +
                "napis “Nie deptać trawy”\n" +
                "napisem szalonym.\n" +
                "(Koniec i początek, 1993)");
    }
}