package pl.edu.uwm. wmii . agnieszkazbrzezny . laboratorium00 ;
import java.util.Scanner;


public class zadanie2_1_h {
    public static void main ( String [] args ) {
        System.out.println("Wpisz n:");
        Scanner s= new Scanner(System.in);
        int n = s.nextInt();
        double wynik = 0;
        for(int i=0;i<n;i++)
        {
            System.out.println("Wpisz " + (i+1) + ". liczbe");
            double temp = s.nextDouble();
            if(i%2==0)
                wynik+=temp;
            else
                wynik-=temp;
        }
        System.out.println(" Wpisano: "+ n + " wyrazow ciagu; wynik = " + wynik);
    }
}