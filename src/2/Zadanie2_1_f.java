package pl.edu.uwm. wmii . agnieszkazbrzezny . laboratorium00 ;
import java.util.Scanner;

import static java.lang.Math.pow;

public class Zadanie2_1_f {
    public static void main ( String [] args ) {
        System.out.println("Wpisz n:");
        Scanner s= new Scanner(System.in);
        int n = s.nextInt();
        double suma = 0;
        for(int i=0;i<n;i++)
        {
            System.out.println("Wpisz " + i + ". liczbe");
            suma+=pow(s.nextDouble(),2);
        }
        System.out.println(" Wpisano: "+ n + " wyrazow ciagu; wynik = " + suma);
    }
}