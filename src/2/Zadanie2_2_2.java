import java.util.Scanner;
public class Zadanie2_2_2 {
    public static void main ( String [] args ) {
        System.out.println("Wpisz n:");
        Scanner s= new Scanner(System.in);
        int n = s.nextInt();
        double suma = 0;
        for(int i=0;i<n;i++) {
            System.out.println("Wpisz " + (i + 1) + ". liczbe");
            double temp = s.nextDouble();
            if(temp>0) suma+=temp;
        }
        System.out.println("Suma =  " + (2*suma));
    }
}