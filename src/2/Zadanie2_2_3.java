import java.util.Scanner;
public class Zadanie2_2_3 {
    public static void main ( String [] args ) {
        System.out.println("Wpisz n:");
        Scanner s= new Scanner(System.in);
        int n = s.nextInt();
        int iloscZer = 0;
        int iloscUjemnych = 0;
        int iloscDodatnich = 0;
        for(int i=0;i<n;i++) {
            System.out.println("Wpisz " + (i + 1) + ". liczbe");
            double temp = s.nextDouble();
            if(temp>0) iloscDodatnich++;
            if(temp==0) iloscZer++;
            if(temp<0) iloscUjemnych++;
        }
        System.out.println("Dodatnich:" + iloscDodatnich + " Ujemnych:" + iloscUjemnych + " Zer:" + iloscZer);
    }
}