import java.util.Scanner;
import java.lang.Math.*;
public class Zadanie2_2_1_h {
    public static void main ( String [] args ) {
        System.out.println("Wpisz n:");
        Scanner s= new Scanner(System.in);
        int n = s.nextInt();
        int ilosc = 0;
        for(int i=0;i<n;i++) {
            System.out.println("Wpisz " + (i + 1) + ". liczbe");
            int temp = s.nextInt();
            if(Math.abs(temp)<(i+1)*(i+1)) ilosc++;
        }
        System.out.println("Istnieje " + ilosc + " takich liczb");
    }
}