package pl.edu.uwm. wmii.agnieszkazbrzezny.laboratorium00 ;
import java.util.Scanner;

public class Zadanie2_2_1_b {
    public static void main ( String [] args ) {
        System.out.println("Wpisz n:");
        Scanner s= new Scanner(System.in);
        int n = s.nextInt();
        int ilosc = 0;
        for(int i=0;i<n;i++)
        {
            System.out.println("Wpisz " + (i+1) + ". liczbe");
            int temp = s.nextInt();
            if(temp%3==0 && temp%5!=0 ) ilosc++;
        }
        System.out.println("Istnieje " + ilosc + " liczb podzielnych przez 3 i nie podzielnych przez 5");
    }


}