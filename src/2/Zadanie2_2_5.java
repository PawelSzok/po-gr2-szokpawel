import java.util.Scanner;
public class Zadanie2_2_5 {
    public static void main ( String [] args ) {
        System.out.println("Wpisz n:");
        Scanner s= new Scanner(System.in);
        int n = s.nextInt();
        int ilosc = 0;
        double [] tablica = new double[n];
        //petla do wczytywania tablicy
        for(int i=0;i<n;i++) {
            System.out.println("Wpisz " + (i + 1) + ". liczbe");
            tablica[i] = s.nextDouble();
        }
        //petla do znajdowania par
        for(int i=1;i<n;i++) {
            if(tablica[i-1]>0 && tablica[i]>0) ilosc++;
        }
        System.out.println("Ilosc par: " + ilosc );
    }
}