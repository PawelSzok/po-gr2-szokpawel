import java.util.Scanner;
import java.lang.Math.*;
public class Zadanie2_2_1_d {
    public static void main ( String [] args ) {
        System.out.println("Wpisz n:");
        Scanner s= new Scanner(System.in);
        int n = s.nextInt();
        int []tablica = new int [n];
        int ilosc = 0;
//         wczytanie do tablicy
        for(int i=0;i<n;i++) {
            System.out.println("Wpisz " + (i + 1) + ". liczbe");
            tablica[i] = s.nextInt();
        }
//         sprawdzanie
        for(int i=1;i<n-1;i++) {
            if (tablica[i] < (tablica[i - 1] + tablica[i + 1]) / 2) ilosc++;
        }
        System.out.println("Istnieje " + ilosc + " liczb spełniajacych warunek");
    }
}