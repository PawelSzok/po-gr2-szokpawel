import java.util.Scanner;
public class Zadanie2_2_4 {
    public static void main ( String [] args ) {
        System.out.println("Wpisz n:");
        Scanner s= new Scanner(System.in);
        int n = s.nextInt();
        double min = 0 ;
        double max = 0;
        for(int i=0;i<n;i++) {
            System.out.println("Wpisz " + (i + 1) + ". liczbe");
            double temp = s.nextDouble();
            // pierwszy przelot petli; ustawienie min/max na pierwsza wartosc
            if(i==0){
                min=temp;
                max=temp;
                continue;
            }
            // a tu juz normalny
            if(temp<min) min=temp;
            else if(temp>max) max=temp;
        }
        System.out.println("Max=:" +max + " Min=" + min );
    }
}