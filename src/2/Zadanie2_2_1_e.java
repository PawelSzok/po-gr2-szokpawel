import pl.edu.uwm.wmii.agnieszkazbrzezny.laboratorium00.Zadanie2_1_i;

import java.util.Scanner;
import java.lang.Math.*;
public class Zadanie2_2_1_e {
    public static void main ( String [] args ) {
        System.out.println("Wpisz n:");
        Scanner s= new Scanner(System.in);
        int n = s.nextInt();
        int ilosc = 0;
//         wczytanie do tablicy
        for(int i=0;i<n;i++) {
            System.out.println("Wpisz " + (i + 1) + ". liczbe");
            int temp = s.nextInt();
            if(Math.pow(2,i)<temp && temp< Zadanie2_1_i.factorial(i)) ilosc++;
        }

        System.out.println("Istnieje " + ilosc + " takich liczb");
    }
}