import java.util.Scanner;

import static java.lang.Math.abs;

public class Zadanie2_1_e {
    public static void main ( String [] args ) {
        System.out.println("Wpisz n:");
        Scanner s= new Scanner(System.in);
        int n = s.nextInt();
        double iloczyn = 1;
        for(int i=0;i<n;i++)
        {
            System.out.println("Wpisz " + i + ". liczbe");
            iloczyn*=abs(s.nextDouble());
        }
        System.out.println(" Wpisano: "+ n + " wyrazow ciagu; wynik = " + iloczyn);
    }
}