package pl.edu.uwm. wmii . agnieszkazbrzezny . laboratorium00 ;
import java.util.Scanner;


public class Zadanie2_1_g {
    public static void main ( String [] args ) {
        System.out.println("Wpisz n:");
        Scanner s= new Scanner(System.in);
        int n = s.nextInt();
        double suma = 0;
        double iloczyn = 1;
        for(int i=0;i<n;i++)
        {
            System.out.println("Wpisz " + i + ". liczbe");
            double temp = s.nextDouble();
            suma+=temp;
            iloczyn*=temp;
        }
        System.out.println(" Wpisano: "+ n + " wyrazow ciagu; suma = " + suma + " iloczyn = "+ iloczyn);
    }
}