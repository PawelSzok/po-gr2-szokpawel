import java.util.ArrayList;
import java.util.Arrays;

public class Zadanie5_3{
    public static ArrayList<Integer> mergeSorted(ArrayList<Integer> a, ArrayList<Integer> b){
        int dlugosc_a= a.size();
        int dlugosc_b= b.size();
        ArrayList<Integer> nowy= new ArrayList<>();
        int i=0;
        int j = Math.min(dlugosc_a, dlugosc_b);
        for(int k=0;k<j;k++){
            nowy.add(a.get(i));
            nowy.add(b.get(i));
            i++;
        }
        if(dlugosc_a<dlugosc_b){
            for(int k=0;k<Math.abs(dlugosc_a-dlugosc_b);k++){
                nowy.add(b.get(i+k));
            }
        }
        else{
            for(int k=0;k<Math.abs(dlugosc_a-dlugosc_b);k++){
                nowy.add(a.get(i+k));
            }
        }

        //sortowanie
        int n = nowy.size();
        int temp = 0;
        for(i=0; i < n; i++){
            for(j=1; j < (n-i); j++){
                if(nowy.get(j-1) > nowy.get(j)){
                    //swap elements
                    temp = nowy.get(j-1);
                    nowy.set(j-1,nowy.get(j));
                    nowy.set(j, temp);
                }

            }
        }
        return nowy;
    }
    public static void main(String[] args){
        ArrayList<Integer> arr1 = new ArrayList<Integer>(Arrays.asList(1,4,9,16));
        ArrayList<Integer> arr2 = new ArrayList<Integer>(Arrays.asList(9,7,4,9,11));
        System.out.println(arr1);
        System.out.println(arr2);
        System.out.println(mergeSorted(arr1,arr2));
    }


}