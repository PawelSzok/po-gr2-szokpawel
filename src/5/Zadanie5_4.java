import java.util.ArrayList;
import java.util.Arrays;

public class Zadanie5_4 {

    public static ArrayList<Integer> reversed(ArrayList<Integer> a) {
        ArrayList<Integer> arrNew = new ArrayList<>();
        for (int i = a.size() - 1; i >= 0; i--) {
            arrNew.add(a.get(i));
        }
        return arrNew;

    }
    public static void main( String[] args){
        ArrayList<Integer> arr1 = new ArrayList<Integer>(Arrays.asList(1,4,9,16));
        System.out.println(reversed(arr1));
    }


}