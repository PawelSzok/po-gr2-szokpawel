
import java.util.ArrayList;
import java.util.Arrays;

public class Zadanie5_2{
    public static ArrayList<Integer> merge(ArrayList<Integer> a, ArrayList<Integer> b){
        int dlugosc_a= a.size();
        int dlugosc_b= b.size();
        ArrayList<Integer> nowy= new ArrayList<>();
        int i=0;
        int j = Math.min(dlugosc_a, dlugosc_b);
        for(int k=0;k<j;k++){
            nowy.add(a.get(i));
            nowy.add(b.get(i));
            i++;
        }
        if(dlugosc_a<dlugosc_b){
            for(int k=0;k<Math.abs(dlugosc_a-dlugosc_b);k++){
                nowy.add(b.get(i+k));
            }
        }
        else{
            for(int k=0;k<Math.abs(dlugosc_a-dlugosc_b);k++){
                nowy.add(a.get(i+k));
            }
        }
        return nowy;
    }
    public static void main(String[] args){
        ArrayList<Integer> arr1 = new ArrayList<Integer>(Arrays.asList(1,4,9,16));
        ArrayList<Integer> arr2 = new ArrayList<Integer>(Arrays.asList(9,7,4,9,11));
        System.out.println(arr1);
        System.out.println(arr2);
        System.out.println(merge(arr1,arr2));
    }

}