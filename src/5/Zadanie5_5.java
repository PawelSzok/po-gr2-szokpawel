import java.util.ArrayList;
import java.util.Arrays;

public class Zadanie5_5 {

    public static void reverse(ArrayList<Integer> a){
        ArrayList<Integer> temp= new ArrayList<>(a);
        int j=0;
        for(int i=temp.size()-1;i>=0;i--) {
            a.set(j,temp.get(i));
            j++;
        }
    }
    public static void main( String[] args){
        ArrayList<Integer> arr1 = new ArrayList<Integer>(Arrays.asList(1,4,9,16));
        System.out.println(arr1);
        reverse(arr1);
        System.out.println(arr1);

    }


}