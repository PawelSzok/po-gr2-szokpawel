import java.util.ArrayList;
import java.util.Arrays;

public class Kolo_zad_2{
    public static ArrayList<Integer> merge(ArrayList<Integer> a, ArrayList<Integer> b) {
        int dlugosc_a = a.size();
        int dlugosc_b = b.size();
        ArrayList<Integer> nowy = new ArrayList<>();
        int m = 0;
        int n = Math.min(dlugosc_a, dlugosc_b);
        for (int k = 0; k < n; k++) {
            nowy.add(a.get(m));
            nowy.add(b.get(m));
            m++;
        }
        if (dlugosc_a < dlugosc_b) {
            for (int k = 0; k < Math.abs(dlugosc_a - dlugosc_b); k++) {
                nowy.add(b.get(m + k));
            }
        } else {
            for (int k = 0; k < Math.abs(dlugosc_a - dlugosc_b); k++) {
                nowy.add(a.get(m + k));
            }
        }
        int dlugosc_nowe = nowy.size();
        for (int i = 0; i < dlugosc_nowe; i++) {
            for (int j = 0; j < i; j++) {
                if (nowy.get(i).equals(nowy.get(j))){
                    nowy.remove(j);
                    dlugosc_nowe-=1;
                }

            }

        }
        return nowy;
    }
        public static void main(String[] args){
            ArrayList<Integer> arr1 = new ArrayList<Integer>(Arrays.asList(4,6,9,16));
            ArrayList<Integer> arr2 = new ArrayList<Integer>(Arrays.asList(4,7,4,9,11));
            System.out.println(arr1);
            System.out.println(arr2);
            System.out.println(merge(arr1,arr2));
    }
}